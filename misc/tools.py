#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 18:57:46 2019

@author: antoine
"""
import numpy as np


class WorldTools:

    def __init__(self):
        pass
    
    def New(self, zoneID, ID, x, y):
        self.ID = ID
        self.zoneID = zoneID
        self.sol = np.ndarray((x,y), dtype=tuple)
        self.decorations = np.ndarray((x,y), dtype=tuple)
        self.blocks = np.ndarray((x,y), dtype=tuple)
        for o in range(x):
            for p in range(y):
                self.sol[o,p] = [None, False]
                self.blocks[o,p] = [False, 'None']
                self.decorations[o,p] = [None, False]
        self.météo = 0   

    
    
    def Load(self, zoneID, ID):
        self.ID = ID
        self.zoneID = zoneID
        
        
        #Pour la signification de ID, zoneID et le chemin d'accès, voir doc/Techniques/Mondes
        data = np.load('../src/app/Data/Map/Zone ' + str(zoneID) + '/' + str(ID) + '.npz', allow_pickle=True)
        self.sol, self.decorations, self.blocks, self.météo = data['sol'], data['decorations'], data['blocks'], data['météo']
     
    
    def Infos(self):
        print("Taille du monde :", self.sol.shape[0], 'x', self.sol.shape[1])
        print("Météo du monde :", self.météo, ' (0 = beau, 1=brouillard)')
    
    def Backup(self):
        self.baksol = np.ndarray((self.sol.shape[0], self.sol.shape[1]), dtype=tuple)
        self.bakdecorations = np.ndarray((self.sol.shape[0], self.sol.shape[1]), dtype=tuple)
        self.bakblocks = np.ndarray((self.sol.shape[0], self.sol.shape[1]), dtype=tuple)
        for x in range(self.sol.shape[0]):
            for y in range(self.sol.shape[1]):
               self.baksol[x,y] = self.sol[x,y]
               self.bakdecorations[x,y] = self.decorations[x,y]
               self.bakblocks[x,y] = self.blocks[x,y]

    def Revert(self):
        self.sol = self.baksol
        self.decorations = self.bakdecorations
        self.blocks = self.bakblocks




    
    def Save(self):
        np.savez_compressed('../src/app/Data/Map/Zone ' + str(self.zoneID) + '/' + str(self.ID) + '.npz', sol = self.sol, decorations=self.decorations, blocks=self.blocks, météo=self.météo)
        
    def Sol(self, coord, block=None):
        print(self.sol[coord[0], coord[1]])
        if block != None:
            self.sol[coord[0], coord[1]] = block
        
        
    def Décorations(self, coord, block=None):
        print(self.decorations[coord[0], coord[1]])
        
        self.decorations[coord[0], coord[1]] = block
        
        
    def Block(self, coord, block=None):
        print(self.blocks[coord[0], coord[1]])
        if block != None:
            self.blocks[coord[0], coord[1]] = block
            
    def Météo(self, météo=None):
        print(self.météo)
        if météo != None:
            self.météo = météo
            
    
    
    def FillSol(self, topLeft, bottomRight, block):
        for x in range(bottomRight[0]-topLeft[0]):
            for y in range(bottomRight[1]-topLeft[1]):
                print(topLeft[0]+x,topLeft[1]+y)
                self.sol[topLeft[0]+x,topLeft[1]+y] = block
    def FillDécorations(self, topLeft, bottomRight, block):
        for x in range(bottomRight[0]-topLeft[0]):
            for y in range(bottomRight[1]-topLeft[1]):
                print(topLeft[0]+x,topLeft[1]+y)
                self.decorations[topLeft[0]+x,topLeft[1]+y] = block
    
    def FillBlocks(self, topLeft, bottomRight, block):            
        for x in range(bottomRight[0]-topLeft[0]):
            for y in range(bottomRight[1]-topLeft[1]):
                print(topLeft[0]+x,topLeft[1]+y)
                self.blocks[topLeft[0]+x,topLeft[1]+y] = block
        
        