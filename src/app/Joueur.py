
import Texture as tx
import sdl2.ext
import numpy as np

from Utils import Input, Events, Data

class Joueur():
    
    
    #Déclaration de toutes les variables et procédure d'initialisation si nécessaires
    def __init__(self, renderer):
        
        self.texturesArrete = list()
        self.texturesWalking = list()
        
        self.renderer = renderer
        
        self.nbr_phaseAnimation = 4
        
        for x in range(4):
            self.texturesArrete.append(tx.GenTexture(renderer, 'Personnages/Joueur', 'joueur', x))
            self.texturesWalking.append(tx.GenAnimatedTexture(renderer, 'Personnages/Joueur', 'joueur' + str(x) + '-', self.nbr_phaseAnimation, 16))

        self.orientation = 1
    
        self.input = Input()
    
        self.mooving = False
        self.canMove = True
        self.phaseAnimation, self.quartAnimationTarget = 0, 0
        
        self.targetTexture = self.texturesArrete[self.orientation]
        self.coord = [20,48]
        self.targetCoord = [20,49]

        self.loaded = False

    #Charge le joueur depuis la sauvegarde
    def Charger(self):
        
        sav = np.load('Data/Sauvegarde/joueur.sav.npz', allow_pickle=True)
        if sav != False:
            
            self.coord = [sav['coord'][0], sav['coord'][1]]
            self.orientation = sav['orientation']
          
        self.loaded = True

    #Sauvegarde le joueur
    def Sauvegarder(self):
        if self.loaded == True:
            np.savez_compressed("Data/Sauvegarde/joueur.sav.npz", coord=self.coord, orientation=self.orientation) 

    #Détruit les objets SDL pour éviter les crashs par corruption de VRAM
    def Decharger(self):
        '''
            Supprime proprement toutes les textures associées aux personnages
        '''
        
        for x in self.texturesArrete:
            tx.DeleteTexture(x)
        for x in self.texturesWalking:
            tx.DeleteTexture(x)

    
    def Update(self, monde):
        
        # sdl2.SDL_GetTicks()+250 : 250 = la durée total de l'animation
        # elf.texturesWalking.append(tx.GenAnimatedTexture(renderer, 'Personnages/Joueur', 'joueur' + str(x) + '-', 4, 16)) : ICI 4 est le nombre d'étapes dans l'animation, et 16 viens de 
        # 250 = 1000/16 * 4, soit la durée d'une des étapes de l'animation  
        
        for x in Events.GetEvents("Joueur"):
            if x[1] == "Tp":
                self.coord = x[2]
            elif x[1] == "LockMouvement":
                self.canMove = not x[2]
            elif x[1] == "Charger":
                self.Charger()
   
        
        if self.input.touchesClavier[Data.GetVariable('toucheAction',Data.Config.data)] == True: #Si le joueur agit avec un pnj, je lance l'action en attente de ce pnj
            x = monde.IsPNJ(self.targetCoord)
            if type(x) == int:
                Events.AddEvent(['PNJ', 'DeclancherAction', x])
        
        #Si le joueur est à l'arrêt et il peu bouger ( = pas en dialogue)
        if self.mooving == False and self.canMove == True: 
     
            #On vérifie la direction du mouvement et le fait bougé
            if self.input.touchesClavier[sdl2.SDL_SCANCODE_UP] : 
                self.targetCoord = [self.coord[0], self.coord[1]-1]
                if monde.GetBlock(self.targetCoord)== False and monde.IsPNJ(self.targetCoord) == False:
                    self.orientation = 2
                    self.mooving = True
                    self.targetTexture = self.texturesWalking[self.orientation]
                    self.endMouvement = sdl2.SDL_GetTicks()+250
                    self.coord[1] -=(1/self.nbr_phaseAnimation)


            elif self.input.touchesClavier[sdl2.SDL_SCANCODE_DOWN] : 
                self.targetCoord = [self.coord[0], self.coord[1]+1]
                if monde.GetBlock(self.targetCoord) == False and monde.IsPNJ(self.targetCoord) == False:
                    self.orientation = 0
                    self.targetTexture = self.texturesWalking[self.orientation]
                    self.mooving = True
                    self.endMouvement = sdl2.SDL_GetTicks()+250
                    self.coord[1]+=(1/self.nbr_phaseAnimation)

            elif self.input.touchesClavier[sdl2.SDL_SCANCODE_RIGHT] : 
                self.targetCoord = [self.coord[0]+1, self.coord[1]]
                if monde.GetBlock(self.targetCoord)== False and monde.IsPNJ(self.targetCoord) == False:
                    self.orientation = 1
                    self.targetTexture = self.texturesWalking[self.orientation]
                    self.mooving = True
                    self.endMouvement = sdl2.SDL_GetTicks()+250
                    self.coord[0] += (1/self.nbr_phaseAnimation)


            elif self.input.touchesClavier[sdl2.SDL_SCANCODE_LEFT] : 
                self.targetCoord = [self.coord[0]-1, self.coord[1]]
                if monde.GetBlock(self.targetCoord)== False and monde.IsPNJ(self.targetCoord) == False:
                    self.orientation = 3
                    self.targetTexture = self.texturesWalking[self.orientation]
                    self.mooving = True
                    self.endMouvement = sdl2.SDL_GetTicks()+250
                    self.coord[0]-= (1/self.nbr_phaseAnimation)
            
        #Update du déplacement si le joeuur est en mouvement
        elif sdl2.SDL_GetTicks() >= self.quartAnimationTarget and self.mooving == True:
            self.phaseAnimation+=1
            
            #Si l'animation est finie, remise du joueur à l'arret
            if self.phaseAnimation >=self.nbr_phaseAnimation:
                self.coord[0], self.coord[1] = int(self.coord[0]), int(self.coord[1])
                self.mooving = False
                self.targetTexture = self.texturesArrete[self.orientation]
                self.phaseAnimation = 0
                
            #Sinon update de l'animation
            else:
                if self.orientation == 1:
                    self.coord[0]+= (1/self.nbr_phaseAnimation)
                elif self.orientation == 3:
                    self.coord[0]-= (1/self.nbr_phaseAnimation)
                elif self.orientation == 2:
                    self.coord[1] -=(1/self.nbr_phaseAnimation)
                else:
                    self.coord[1]+=(1/self.nbr_phaseAnimation)
                
                    
                self.quartAnimationTarget = sdl2.SDL_GetTicks() + 1000/16
                        
    #Affiche le joueur
    def Afficher(self, taille, compX, compY):
        '''
            Affiche le sprite du joueur
        '''        
        #Si la caméra est fixé au joueur
        if self.input.cameraLocked == True:
            
            rD = Data.GetVariable("renderDistance", Data.Config.data)
            rD = rD//2
            tx.AfficherTexture(self.renderer, self.targetTexture, [rD, rD], taille, compX, compY)
        
        #Sinon elle est centré sur le centre du monde
        else:
            tx.AfficherTexture(self.renderer, self.targetTexture, [self.coord[0], self.coord[1]], taille, compX, compY)
        
