#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 14:32:43 2019

@author: antoine
"""

#Pour charger et manipuler le monde sous forme de tableau bi-dimensionnel
import numpy as np
#Pour charger des textures
import Texture as tx
#Pour afficher les textures et la météo
import sdl2.ext
#Pour utiliser le framework d'évènement, la config et les entrés clavier
from Utils import Input, Events, Data
#Pour les personnages non joueur
#import PNJ

from Monde import Météo

class Monde():            
    
    #Routine d'initialisation
    def __init__(self, renderer):
        self.renderer = renderer
                
        self.sol = np.ndarray((30,30), dtype=tuple)
        self.decorations = np.ndarray((30,30), dtype=tuple)
        self.blocks = np.ndarray((30,30), dtype=tuple)
        
        self.météo = Météo(renderer)


        self.loaded = False
        self.waitingLoading = False
        self.triggerLoadTime = 0
    
        #self.test_pnj = PNJ.PNJ(renderer, 1)
    
    #Chargement d'une zone (intérieur d'un batiment)
    def Charger(self, zoneID, ID):
        '''
            Chagre une zone déja générée
        '''
                
        while self.loaded == True:
            self.Sauvegarder()
            self.Decharger()
         
        self.ID = ID
        self.zoneID = zoneID
        
        
        #Pour la signification de ID, zoneID et le chemin d'accès, voir doc/Techniques/Mondes
        data = np.load('Data/Map/Zone ' + str(zoneID) + '/' + str(ID) + '.npz', allow_pickle=True)
        self.sol, self.decorations, self.blocks= data['sol'], data['decorations'], data['blocks']
        
        self.météo.Update( data['météo'])
       
        # Vérouillage de la caméra sur le joueur
        Events.AddEvent(['Monde', 'LockCamera', True])
        
        #Création des tableaux contenant les textures 
        self.solTextures = dict()
        self.decorationsTextures = dict()

        #Chargement des textures
        for x in range(self.sol.shape[0]):
            for y in range(self.sol.shape[1]):
                
                if self.sol[x,y][0]!= None:
                    try:
                        self.solTextures[self.sol[x,y][0]]
                    except KeyError:
                        if self.sol[x,y][1] != True:
                            self.solTextures[self.sol[x,y][0]] = tx.GenTexture(self.renderer, 'Sol', self.sol[x,y][0])
                        else: 
                            self.solTextures[self.sol[x,y][0]] = tx.GenAnimatedTexture(self.renderer, 'Sol', self.sol[x,y][0], self.sol[x,y][2], self.sol[x,y][3])
                      
                
                if self.decorations[x,y][0] != None:
                    try:
                        self.decorationsTextures[self.decorations[x,y][0]]
                    except KeyError:
                       if self.decorations[x,y][1] != True:
                           self.decorationsTextures[self.decorations[x,y][0]] = tx.GenTexture(self.renderer, 'Décorations', self.decorations[x,y][0])
                       else:
                           self.decorationsTextures[self.decorations[x,y][0]] = tx.GenAnimatedTexture(self.renderer, 'Décorations',self.decorations[x,y][2], self.decorations[x,y][3])

        self.loaded = True
   
        
    #Déchargement du monde
    def Decharger(self):
        '''
            Supprime les textures SDL pour éviter les crashs par corruption de vram
        '''
        if self.loaded ==True:
            for x in range(self.sol.shape[0]):
                for y in range(self.sol.shape[1]):
                    if self.decorations[x,y][0] != None:
                        try:
                            tx.DeleteTexture(self.decorationsTextures[self.decorations[x,y][0]])
                        except KeyError:
                            pass
                    if self.sol[x,y][0] != None:
                        try:
                            tx.DeleteTexture(self.solTextures[self.sol[x,y][0]])
                        except KeyError:
                            pass
                        
        self.loaded = False
    
    #Sauvegarde de l'état du monde             
    def Sauvegarder(self):
        '''
            Sauvegarde le monde dans un fichier archive
        '''    
        if self.loaded == True:
            np.savez_compressed("Data/Sauvegarde/monde.sav.npz", zoneID=self.zoneID, ID=self.ID) 
    
    def GetBlock(self, coord):
        '''
            Renvoie le booléen pour savoir s'il y a un block solide (mur à la hauteur du joueur)
        '''        
                
        if self.blocks[coord[0], coord[1]][1] == 'warp': #Si le joueur essaye d'aller sur un block téléporteur, on lance l'evènement
            
            Events.AddEvent(['Monde', 'Charger', self.blocks[int(coord[0]), int(coord[1])][2], self.blocks[int(coord[0]), int(coord[1])][3], self.blocks[int(coord[0]), int(coord[1])][4]  ])
        
        elif  self.blocks[int(coord[0]), int(coord[1])][1] == 'trigger':
            for x in  self.blocks[int(coord[0]), int(coord[1])][2]:
                print(coord)
                Events.AddEvent(x)
        
        return self.blocks[coord[0], coord[1]][0]    
    
    
    def IsPNJ(self, coord):
        return False
    
    def Update(self):
        '''
            Met à jour le monde (gestion des évènements càd le chargement / sauvegarde / évolution du monde)
        '''          
        self.météo.Update()             
        
        for x in Events.GetEvents('Monde'): #Si il y une demande de chargement de zone
            if x[1] == 'Charger':
                Events.AddEvent(["UI", "TransitionStart"]) #On lance l'animation puis setup l'attente pour lancer le chargement lors de la fin de l'animation
                self.waitingLoading = True
                self.triggerLoadTime = sdl2.SDL_GetTicks()+550
                self.toLoadID = [x[2],x[3]]
                self.toLoadSpawn = x[4]
                Events.AddEvent(['Son', 'StopMusique', 450])

            elif x[1] == "UpdateMusique":
                Events.AddEvent(['Son', 'PlayMusique', str(self.zoneID) + '_' + str(self.ID) + '_' + str(x[2]), 500,500])

            elif x[1] == "SetMétéo":
                self.météo.Update(x[2])
                
            elif x[1] == "LockCamera":
                Input.cameraLocked = x[2]
        
        #self.test_pnj.Update()
                      
        if sdl2.SDL_GetTicks() >= self.triggerLoadTime and self.waitingLoading == True: #Lorsque le dalais de l'animation est finie :
            self.waitingLoading = False
            self.Charger(self.toLoadID[0], self.toLoadID[1]) #On lance le chargement puis quand c fini
            if self.toLoadSpawn != None:
                self.spawn = self.toLoadSpawn
                Events.AddEvent(["Joueur", "Tp", self.toLoadSpawn])
            
            Events.AddEvent(["UI", "TransitionEnd"]) #On envoie l'animation de fin de transition
       
    '''FONCTION DE RENDU '''   
      
    def Afficher1(self, taille, compX, compY, coordJoueur):
        '''
            Affiche la 1ere partie (sol) du monde dans dans le renderer en tempon
        '''
 
        if self.loaded == True:     
            #Calcul des coordonnées limites de la zone à afficher avec protection limite
            rD = Data.GetVariable("renderDistance", Data.Config.data)
            temp = [int(coordJoueur[0]-3)-int(rD/2), int(coordJoueur[1])-int(rD/2)-3]      
            temp2 = [temp[0]+rD+6, temp[1]+rD+6]
            
            #Vérification que on ne doit pas afficher des blocks en dehors du monde
            if temp[0] < 0:
                temp[0] = 1 
            if temp[1] < 0:
                temp[1] = 0    
            if temp2[0] > self.sol.shape[0]:
                temp2[0] = self.sol.shape[0]
            if temp2[1] > self.sol.shape[1]:
                temp2[1] = self.sol.shape[1]
                        
            
            if Input.cameraLocked == False:            
                for x in range(temp[0], temp2[0]):
                    for y in range(temp[1], temp2[1]):
                        if self.sol[x,y][0] != None:
                            tx.AfficherTexture(self.renderer, self.solTextures[self.sol[x,y][0]], [x,y], taille, compX, compY)
        
                for x in range(temp[0], temp2[0]):
                    for y in range(temp[1], temp2[1]):
                        if self.decorations[x,y][0] != None:
                            tx.AfficherTexture(self.renderer, self.decorationsTextures[self.decorations[x,y][0]], [x,y], taille, compX, compY)
            else:
                
                for x in range(temp[0], temp2[0]):
                    for y in range(temp[1], temp2[1]):
                        if self.sol[x,y][0] != None:
                            tx.AfficherTexture(self.renderer, self.solTextures[self.sol[x,y][0]], [x+((rD//2)-coordJoueur[0]),y+((rD//2)-coordJoueur[1])], taille, compX, compY)
        
                for x in range(temp[0], temp2[0]):
                    for y in range(temp[1], temp2[1]):
                        if self.decorations[x,y][0] != None:
                            tx.AfficherTexture(self.renderer, self.decorationsTextures[self.decorations[x,y][0]], [x+((rD//2)-coordJoueur[0]),y+((rD//2)-coordJoueur[1])], taille, compX, compY)
                
    
    def Afficher2(self, taille, compX, compY, coordJoueur):
        '''
            Affiche la 2eme partie (Todo: objet en hauteur) météo, du monde dans dans le renderer en tempon
        '''
        
        #self.test_pnj.Afficher(taille, compX, compY, coordJoueur)
        self.météo.Afficher(taille, compX, compY)

            