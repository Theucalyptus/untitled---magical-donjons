#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from Utils import Input, Events
import sdl2.ext
import Texture as tx
import random

class Météo():
    
 
    couleurs = (sdl2.SDL_Color(255,255,255,90), sdl2.SDL_Color(10,2,15,90))
    
    def __init__(self, renderer):
        self.renderer = renderer
        self.météo = 0
        self.changeMeteoTarget = None
        self.changeMeteoTrigger = 0

        
        self.textureParticule = tx.GenTexture(self.renderer, 'Particules', 'flocon')
        self.particules = [None] * 400            
        
    def Update(self, météoID=None):
       
        if météoID != None:
            
            if météoID == 2:         
                if random.randint(0,100) >= 90: #Pluie aléatoire
                    self.météo = 0
                else:
                    self.météo = 2
                
                self.changeMeteoTarget = sdl2.SDL_GetTicks() + random.randint(60000, 100000) #Durant 5 à 10 min

            else:
                self.météo = météoID
            
            Events.AddEvent(['Monde', 'UpdateMusique', self.météo])
            tx.DeleteTexture(self.textureParticule)      
            
            if self.météo == 1:
                self.lowSpeed = 0.0005
                self.HighSpeed = 0.015 
                self.direction = 0.15
                self.textureParticule = tx.GenTexture(self.renderer, 'Particules', 'flocon')
                                
            elif self.météo == 2:
                self.lowSpeed = 0.01
                self.HighSpeed = 0.03
                self.direction = 0.08
                self.textureParticule = tx.GenTexture(self.renderer, 'Particules', 'droplet')
                
            if self.météo != 0:
                for x in range(len(self.particules)):
                    self.particules[x] = [random.uniform(-self.direction,self.direction), random.uniform(self.lowSpeed,self.HighSpeed), random.randint(-2000,-10), random.uniform(0, 100.0)]




        if  self.changeMeteoTarget != None and self.changeMeteoTarget <= sdl2.SDL_GetTicks(): #Désactivation de la pluie
            if self.météo == 2:
                self.météo = 0
            else:
                self.météo = 2
            Events.AddEvent(['Monde', 'UpdateMusique', self.météo])
            self.changeMeteoTarget = sdl2.SDL_GetTicks() + random.randint(60000, 100000) #Durant 5 à 10 min

       
        if self.météo != 0:     
           #Mise à jour de l'avencement des particules
           for x in range(len(self.particules)):
               self.particules[x][2] = self.particules[x][2]+self.particules[x][1]*Input.resY
               if self.particules[x][2] >= Input.resY*1.5:
                   self.particules[x] = [random.uniform(-self.direction,self.direction), random.uniform(self.lowSpeed,self.HighSpeed), random.randint(-500,-10), random.uniform(0, 100.0)]


    def GetMétéoID(self):
        return self.météo
        
    
    def Afficher(self, taille, compX, compY):

        if self.météo != 0:
            for x in range(len(self.particules)):
                tx.AfficherTexture(self.renderer, self.textureParticule, [(self.particules[x][0]*self.particules[x][2]+self.particules[x][3]*Input.resX/100)/taille,(self.particules[x][2])/taille], taille, compX, compY)
        
            model = sdl2.SDL_Rect(0,0, Input.resX, Input.resY)

            if self.météo == 1: # Neige
                color = self.couleurs[0]
            elif self.météo == 2: #Nuit, orage, pluie A DETERMINER
                color = self.couleurs[1]
                 
            sdl2.SDL_SetRenderDrawColor(self.renderer, color.r, color.g, color.b, color.a)
            sdl2.SDL_RenderFillRect(self.renderer, model)
            
