
import sdl2.ext
from Utils import Input
from math import floor

class Texture():
    
    def __init__(self, SDL_Texture, forme, taille):
        '''
            SDL_Texture est de type SDL_Texture
        '''
        
        self.targetTexture = SDL_Texture
        self.forme = forme
        self.taille = taille


class AnimatedTexture():
    
    def __init__(self, SDL_Textures, forme, index, vitesse):
        '''
            SDL_Textures est une !liste! de texture SDL
        '''
        self.targetTexture = SDL_Textures[index]
        self.textures = SDL_Textures
        self.forme = forme
        self.index = index
        self.updateFrequency = 1000//vitesse
        self.updateTarget = self.updateFrequency
        
        
    def Update(self):
        
        if sdl2.SDL_GetTicks() >= self.updateTarget and Input.pause != True:
            self.index +=1
            if self.index >= len(self.textures):
                self.index = 0
            self.targetTexture = self.textures[self.index]
            self.updateTarget = sdl2.SDL_GetTicks() + self.updateFrequency
        
        
def GenTexture(renderer, categorie, texture, variante=0):
    if variante == 0:
        tempTexture1 = sdl2.ext.load_image("Data/Textures/" + categorie + "/" + texture + ".png")     
     
    else:    
        tempTexture1 = sdl2.ext.load_image("Data/Textures/" + categorie + "/" + texture + str(variante) + ".png")     
    texture = sdl2.SDL_CreateTextureFromSurface(renderer, tempTexture1)                
    forme = (tempTexture1.w//64, tempTexture1.h//64)
    taille = (tempTexture1.w, tempTexture1.h)
    sdl2.SDL_FreeSurface(tempTexture1)
    
    return Texture(texture, forme, taille)
    

def GenAnimatedTexture(renderer, categorie, texture, nbrphase, vitesse, pos_Initiale=0):
    
    textures = list()
    
    for x in range(0, nbrphase):
        if x == 0:
            tempTexture1 = sdl2.ext.load_image("Data/Textures/" + categorie + "/" + texture + ".png") 
        else: 
            tempTexture1 = sdl2.ext.load_image("Data/Textures/" + categorie + "/" + texture + str(x+1) + ".png") 

        textures.append(sdl2.SDL_CreateTextureFromSurface(renderer, tempTexture1))                
        forme = (tempTexture1.w//64, tempTexture1.h//64)
        sdl2.SDL_FreeSurface(tempTexture1)
        
    return AnimatedTexture(textures, forme, pos_Initiale, vitesse)
    


def AfficherTexture(renderer, texture, coord, taille, compX, compY, ByPassSize=False):
    
    if ByPassSize == True:
        model = sdl2.SDL_Rect(int(coord[0]*taille)-3,int(coord[1]*taille)-5, Input.resX+7, Input.resY+5)
        crop = None
    else:
        crop = None
        model = sdl2.SDL_Rect((floor(coord[0])*taille-taille//2)-int(compX*taille/2) + int((coord[0]-floor(coord[0]))*taille), (floor(coord[1])*taille-taille//2)-int(compY*taille/2) + int((coord[1]-floor(coord[1]))*taille), taille*texture.forme[0], taille*texture.forme[1])
        
    sdl2.SDL_RenderCopy(renderer, texture.targetTexture, crop, model)
    
    if isinstance(texture, AnimatedTexture):
        texture.Update()
    
    
def DeleteTexture(texture):
    sdl2.SDL_DestroyTexture(texture.targetTexture)
    
    if isinstance(texture, AnimatedTexture):
        for x in texture.textures:
            sdl2.SDL_DestroyTexture(x)
    