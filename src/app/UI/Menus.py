#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 28 11:04:54 2019

@author: antoine
"""

from UI import UI_Bouton, UI_CheckBox
from Utils import Events

class UI_MenuPrincipal():
    
    def __init__(self, renderer):
          self.renderer = renderer
        
          self.boutonJouer = UI_Bouton(self.renderer, "Joueur", [45,45], event=[["Main", "Jouer"]])
          self.boutonQuitter = UI_Bouton(self.renderer, "Quitter", [45,59], event=[["Main", "Quitter"]])
          self.boutonOptions = UI_Bouton(self.renderer, "Options", [45,52], event=[["UI", "MenuPrincipalShow", False], ["UI", "MenuOptionsShow", True]])

          Events.AddEvent(['Son', 'PlayMusique', 'menu1', 500, 0])
          
          
    def Update(self):
        self.boutonJouer.Update()
        self.boutonQuitter.Update()
        self.boutonOptions.Update()
            
        
    def Afficher(self):
                    
        self.boutonJouer.Afficher()
        self.boutonOptions.Afficher()
        self.boutonQuitter.Afficher()
        
        
        
class UI_MenuOptions():
    
    def __init__(self, renderer):
        self.renderer = renderer

        self.boutonToucheAction = UI_Bouton(self.renderer, "Touche Action", [45,48], event=[["Input", "SetKeyFor", 'toucheAction']])            
        self.boutonToucheRetour = UI_Bouton(self.renderer, "Touche Retour", [45,55], event=[["Input", "SetKeyFor", 'toucheRetour']])
        self.boutonToucheCarte = UI_Bouton(self.renderer, "Touche Carte", [45,62], event=[["Input", "SetKeyFor", 'toucheCarte']])  
        self.checkBoxFullscreen = UI_CheckBox(self.renderer, [45,69], 'Plein Écran', 'fullScreen')
        
        self.boutonRetour = UI_Bouton(self.renderer, "Retour", [80,73], event=[["UI", "MenuOptionsShow", False], ["UI", "MenuPrincipalShow", True]]) 
        self.boutonAppliquer = UI_Bouton(self.renderer, "Appliquer", [80,80], event=[['Main', 'AppliquerConfig']])
        
    def Update(self):
        
        self.boutonRetour.Update()
        self.boutonAppliquer.Update()

        
        self.boutonToucheAction.Update()
        self.boutonToucheRetour.Update()
        self.boutonToucheCarte.Update()

        
        self.checkBoxFullscreen.Update()
        
    def Afficher(self):
        self.boutonRetour.Afficher()
        self.boutonAppliquer.Afficher()

        
        self.boutonToucheAction.Afficher()
        self.boutonToucheRetour.Afficher()
        self.boutonToucheCarte.Afficher()

        self.checkBoxFullscreen.Afficher()