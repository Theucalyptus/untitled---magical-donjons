#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 28 15:14:28 2019

@author: antoine
"""
import sdl2.ext
import Texture as tx

from Utils import Input


class UI_ElementBackground():
    
    def __init__(self, renderer, coord, sizeX, sizeY, marge=7,  border=False):
        
        self.renderer = renderer
        self.coord, self.marge, self.sizeX, self.sizeY = coord, marge, sizeX, sizeY
        
        self.bg_colorDefault = sdl2.SDL_Color(115, 113, 108, 127)
        self.bg_colorHoovered = sdl2.SDL_Color(230, 226, 218, 127)
        
        self.borderEnable = border
        self.borderColorDefault = sdl2.SDL_Color(000,000,000,255)
        self.borderColorHoovered = sdl2.SDL_Color(255,255,255,255)
        
        
    def Update(self, hoovered=False):
        
        self.model = sdl2.SDL_Rect(int((self.coord[0]/100*Input.resX)-self.marge),int((self.coord[1]/100*Input.resY)-self.marge), int(self.sizeX+self.marge*2), int(self.sizeY+self.marge*2)     )         
        self.border = sdl2.SDL_Rect(int((self.coord[0]/100*Input.resX)-self.marge),int((self.coord[1]/100*Input.resY)-self.marge), int(self.sizeX+self.marge*2), int(self.sizeY+self.marge*2)     )

        
        
        if hoovered == False:
            self.bg_color = self.bg_colorDefault
            self.borderColor = self.borderColorDefault
        else:
            self.bg_color = self.bg_colorHoovered
            self.borderColor = self.borderColorHoovered
            
    def Afficher(self):
        
        sdl2.SDL_SetRenderDrawColor(self.renderer, self.bg_color.r, self.bg_color.g, self.bg_color.b, self.bg_color.a)
        sdl2.SDL_RenderFillRect(self.renderer, self.model)
        
        if self.borderEnable == True:
            sdl2.SDL_SetRenderDrawColor(self.renderer, self.borderColor.r, self.borderColor.g, self.borderColor.b, self.borderColor.a)
            sdl2.SDL_RenderDrawRect(self.renderer, self.border)

            

            
            
            
def UI_GenTextureFromText(renderer, texte, color=None):
    
    police = sdl2.sdlttf.TTF_OpenFont(str("Data/Polices/roboto.ttf").encode('utf-8'), 20)
    if color ==None:
        color = sdl2.SDL_Color(255,255,255,255)
    
    texteSurface = sdl2.sdlttf.TTF_RenderUTF8_Blended(police, texte.encode('utf-8'), color)
    sizeX, sizeY = texteSurface.contents.w, texteSurface.contents.h
    
    sdl2.sdlttf.TTF_CloseFont(police)

    targetTexture = sdl2.SDL_CreateTextureFromSurface(renderer, texteSurface)    

     
    sdl2.SDL_FreeSurface(texteSurface)
    return targetTexture, sizeX, sizeY


def UI_BackgroundImage(texture, taille, centrageVerticale=1):
    '''
        centrage = 0 : centré sur le haut
        centrage = 1 : centré sur le milieu
        centrage = 2 : centré sur le bas
    '''
   
    def __init__(self):
        self.texture = texture
        self.centrageVerticale = centrageVerticale
        
        self.picSize = [ texture.forme[0]*taille, texture.forme[1]*taille]
        print(self.picSize)
        
    def Afficher(self, compX, compY):
        
        tx.AfficherTexture(self.renderer, self.background, [0,0], taille, compX, compY*self.centrageVerticale)
