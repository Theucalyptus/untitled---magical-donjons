#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 09:49:35 2019

@author: antoine
"""

import sdl2.ext


from Utils import Input, Events, Data

from UI import UI_ElementBackground, UI_GenTextureFromText


class UI_Bouton():
    
    def __init__(self,renderer,  texte, coord, visible=True, event=[]):
        '''
            Créer un bouton avec un texte définie, des coordonnées définie, et optionnellement une taille
        '''
        
        self.renderer = renderer
        self.targetTexture = None
        self.marge = 7

        self.SetText(texte)
        self.coord = coord

        self.events = event      
        self.background = UI_ElementBackground(renderer, coord, self.sizeX, self.sizeY, marge=self.marge,border=True)
        
        
    def SetText(self, texte):
        """
            texte est de type string, contient le texte à afficher sur le bouton
        """

        if self.targetTexture != None:
            sdl2.SDL_DestroyTexture(self.targetTexture)
        
        self.targetTexture, self.sizeX, self.sizeY = UI_GenTextureFromText(self.renderer, texte)
        
    def Update(self):

        if Input.sourisX >= self.coord[0]/100*Input.resX and Input.sourisX <= self.coord[0]/100*Input.resX+self.sizeX*(1+self.marge/100) and Input.sourisY >= self.coord[1]/100*Input.resY and Input.sourisY <= self.coord[1]/100*Input.resY+self.sizeY*(1+self.marge/100):
            self.background.Update(hoovered=True)
            
            if Input.touchesSouris[1] == True and len(self.events)>0:
                Input.touchesSouris[1] = False
                for x in range(len(self.events)):
                    Events.AddEvent(self.events[x])
                   
        else:
            self.background.Update(hoovered=False)
    
    
    def Afficher(self):
                    
        #Affichage de l'arrière plan du bouton
        self.background.Afficher()     
                
        #Affichage du texte à l'intérieur
        model = sdl2.SDL_Rect(int(self.coord[0]/100*Input.resX),int(self.coord[1]/100*Input.resY),self.sizeX,self.sizeY)
        sdl2.SDL_RenderCopy(self.renderer, self.targetTexture, None, model)
        


class UI_BoiteTexte():
    
    def __init__(self, renderer, coord, visible=True):
        
        self.sizeX = 500
        self.sizeY= 50
        self.coord = [(Input.resX/2-self.sizeX/2)/Input.resX*100,80]

        
        self.termine = True
        self.ID = -1
        self.ActionID = -1
        self.lock = True
         
        self.renderer = renderer
        self.targetTexture = None
        self.background = UI_ElementBackground(renderer, self.coord, self.sizeX, self.sizeY, border=True)

        self.startAnimation = sdl2.SDL_GetTicks()
        
        self.charMedLen = 10
        
    def SetTexte(self, texte, nom = "", ID=None, ActionID=None):
        self.background.Update()
        self.termine = False  
        self.startAnimation = sdl2.SDL_GetTicks()        

        self.ID = ID
        self.ActionID = ActionID
        self.lock = False

        Events.AddEvent(['Joueur', 'LockMouvement', True])

        texte = nom + ": " + texte        

        if self.targetTexture != None:
            if type(self.targetTexture) == list:
                   for x in self.targetTexture:
                        sdl2.SDL_DestroyTexture(x)
            else:
                    sdl2.SDL_DestroyTexture(self.targetTexture)
                    
        if len(texte)*self.charMedLen >= self.sizeX: #Si le message est trop long
                
            for x in range(len(texte)): #Déterminons dans la chaine de caratère le dernier espace avant le retour à la ligne
                if texte[x] == ' ' and self.charMedLen*x <= self.sizeX:
                    index_last_espace = x
            
            texte1 = texte[0:index_last_espace] #Découpage du texte en 2 partie
            texte2= texte[index_last_espace+1:len(texte)]
            
            self.targetTexture, self.sizeTexteX, self.sizeTexteY = UI_GenTextureFromText(self.renderer, texte1)
            self.targetTexture, self.sizeTexteX, self.sizeTexteY = [self.targetTexture], [self.sizeTexteX], [self.sizeTexteY]
            targetTexture2, sizeTexteX2, sizeTexteY2 = UI_GenTextureFromText(self.renderer, texte2)      
            self.targetTexture.append(targetTexture2)
            self.sizeTexteX.append(sizeTexteX2)
            self.sizeTexteY.append(sizeTexteY2)
        
        else:
            self.targetTexture, self.sizeTexteX, self.sizeTexteY = UI_GenTextureFromText(self.renderer, texte)

    
    def Update(self):
        '''
            Met à jour l'animation / les évènements de la boite de dialogue
        '''
        
        self.background.Update()
            
           
        if self.termine == True and self.lock == False and Input.touchesClavier[Data.GetVariable('toucheAction', Data.Config.data)] == True:         
            print("Test2")
            self.lock = True
            if self.ID != None:
                Events.AddEvent(['PNJ', 'FinAction', self.ID, self.ActionID])
            
            Events.AddEvent(['Joueur', 'LockMouvement', False])

        
        elif Input.touchesClavier[Data.GetVariable('toucheAction', Data.Config.data)] == True and self.termine == False:
            print("Test")
            Input.touchesClavier[Data.GetVariable('toucheAction', Data.Config.data)] = False
            self.startAnimation = -1000
            self.termine = True

    
    def Afficher(self):
        '''
            Affiche la boite de dialogue
        '''
        
        if self.lock == False:
            self.background.Afficher()
            
            comp = (sdl2.SDL_GetTicks()-self.startAnimation)//2
                
            if type(self.sizeTexteX) == list:
                comp = [comp]
                comp.append(0)
                if comp[0] >= self.sizeTexteX[0]:
                    comp[1] = comp[0]-self.sizeTexteX[0]
                    comp[0] = self.sizeTexteX[0]
                        
                    if comp[1] >= self.sizeTexteX[1]:
                        comp[1] = self.sizeTexteX[1]
                        self.termine = True
                    
            elif comp >= self.sizeTexteX:
                comp = self.sizeTexteX
                self.termine = True

            if type(self.targetTexture) == list:
                for x in range(0,2):
                    model = sdl2.SDL_Rect(int(self.coord[0]/100*Input.resX),int(self.coord[1]/100*Input.resY)+x*20,comp[x],self.sizeTexteY[x])
                    sdl2.SDL_RenderCopy(self.renderer, self.targetTexture[x], sdl2.SDL_Rect(0,0,comp[x], self.sizeTexteY[x]), model)
            else:
                model = sdl2.SDL_Rect(int(self.coord[0]/100*Input.resX),int(self.coord[1]/100*Input.resY),comp,self.sizeTexteY)
                sdl2.SDL_RenderCopy(self.renderer, self.targetTexture, sdl2.SDL_Rect(0,0,comp, self.sizeTexteY), model)

        
        
        
class UI_CheckBox():
    
    def __init__(self, renderer, coord, titre, variable):
        
        self.renderer = renderer
        self.variable = variable
        self.checked = Data.Config.data[self.variable]
        
        self.coordTexte = coord
        self.TexteTexture, self.sizeTexteX, self.sizeTexteY = UI_GenTextureFromText(self.renderer, titre)


        self.coordCase = [self.coordTexte[0]+110*(self.sizeTexteX/Input.resX), coord[1]]
        self.background = UI_ElementBackground(renderer, self.coordCase, 20, 20, border=True)
        self.background.Update()    
        self.marge = 0
        
        
    def Update(self):
        
        
        if Input.sourisX >= self.coordCase[0]/100*Input.resX and Input.sourisX <= self.coordCase[0]/100*Input.resX+20*(1+self.marge/100) and Input.sourisY >= self.coordCase[1]/100*Input.resY and Input.sourisY <= self.coordCase[1]/100*Input.resY+20*(1+self.marge/100):     
            self.background.Update(hoovered=True)
            
            if Input.touchesSouris[1] == True:
                Input.touchesSouris[1] = False
                Data.Config.data_buffer[self.variable] = not Data.Config.data_buffer[self.variable]
        else:
            self.background.Update()
        
        self.checked = Data.Config.data_buffer[self.variable]
        
        
    def Afficher(self):
        
        model = sdl2.SDL_Rect(int(self.coordTexte[0]/100*Input.resX),int(self.coordTexte[1]/100*Input.resY),self.sizeTexteX,self.sizeTexteY)
        sdl2.SDL_RenderCopy(self.renderer, self.TexteTexture, None, model)
        
        
        self.background.Afficher()

        if self.checked == True:        
            sdl2.SDL_SetRenderDrawColor(self.renderer, 255,255,255,255)
            sdl2.SDL_RenderDrawLine(self.renderer, int(self.coordCase[0]/100*Input.resX), int(self.coordCase[1]/100*Input.resY), int(self.coordCase[0]/100*Input.resX+20*(1+self.marge/100)), int(self.coordCase[1]/100*Input.resY+20*(1+self.marge/100)) )  
            sdl2.SDL_RenderDrawLine(self.renderer, int(self.coordCase[0]/100*Input.resX), int(self.coordCase[1]/100*Input.resY+20*(1+self.marge/100)),int(self.coordCase[0]/100*Input.resX+20*(1+self.marge/100)),int(self.coordCase[1]/100*Input.resY)  )

        

        
        
        
        
        
        
        
        
        
        
        
        
        
        