#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 09:47:09 2019

@author: antoine
"""

from Utils import Events
from UI import UI_MenuPrincipal, UI_MenuOptions, UI_Transition, UI_OverlayTexte, UI_OverlayPause, UI_OverlayMap

import sdl2.ext
import Texture as tx

class UI_Main():
    
    def __init__(self, renderer):
        self.renderer = renderer
        
        self.transition = UI_Transition(renderer, sdl2.SDL_Color(0,0,0,255), 500)   
        
        self.background = tx.GenTexture(renderer, "Menus", "background")
        self.backgroundVisible = True
        self.backgroundVisibleTarget = 0
        
        self.menus = [UI_MenuPrincipal(renderer), UI_MenuOptions(renderer)]
        self.menusVisible = [True, False]

        self.overlays = [UI_OverlayTexte(renderer), UI_OverlayPause(renderer), UI_OverlayMap(renderer)]
        self.overlaysVisible = False        
        
    
    def Update(self):

        for x in Events.GetEvents("UI"):
            if x[1] == "TransitionStart":
                self.transition.StartAnimation()
                
            elif x[1] == "TransitionEnd":
                self.transition.EndAnimation()
                
            elif x[1] == "MenuPrincipalShow":
                self.menusVisible[0] = x[2]
                if x[2] == True:
                    self.backgroundVisible = True
                else:
                    self.backgroundVisibleTarget = sdl2.SDL_GetTicks()+500
                    
                    
            elif x[1] == "MenuOptionsShow" : 
                self.menusVisible[1] = x[2]
                if x[2] == True:
                    self.backgroundVisibleTarget = 0 #Neutralisation du masquage du fond en attente lorsqu'en provenance du menu principal
                

            elif x[1] == "OverlaysShow":
                self.overlaysVisible = x[2]

        #Temporisation de la disparition du fond lors d'une transition
        if self.backgroundVisibleTarget <= sdl2.SDL_GetTicks() and self.backgroundVisibleTarget != 0:
            self.backgroundVisible = False
            self.backgroundVisibleTarget = 0


        for x in range(len(self.menus)):
             if self.menusVisible[x] == True:
                self.menus[x].Update()
                
        if self.backgroundVisible == False:
            for x in range(len(self.overlays)):
                self.overlays[x].Update()
    
        
    def Afficher(self, taille, compX, compY):
        
        if self.backgroundVisible == True:
            tx.AfficherTexture(self.renderer, self.background, [0,0], taille, compX, compY,  ByPassSize=True)  
            
        for x in range(len(self.menus)):
            if self.menusVisible[x] == True:      
                self.menus[x].Afficher()
        
        if self.backgroundVisible == False:
            for x in range(len(self.overlays)):
                self.overlays[x].Afficher(taille, compX, compY)
        
        self.transition.Afficher()
        