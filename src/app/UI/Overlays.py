#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 29 15:10:36 2019

@author: antoine
"""

from UI import UI_Bouton, UI_BoiteTexte, UI_ElementBackground, UI_GenTextureFromText

from Utils import Events, Input, Data

import Texture as tx
import sdl2.ext

class UI_OverlayTexte():
    
    def __init__(self, renderer):
                
        self.boite = UI_BoiteTexte(renderer, coord=[45,80])  
        self.boite.Update()
        self.boiteVisible = False

    def Update(self):
         
        for x in Events.GetEvents("UI_Overlay"):
            if x[1] == "TexteShow":
                 self.boiteVisible = x[1]
            
            elif x[1] == "SetTexte":
                 self.boiteVisible = True
                 self.boite.SetTexte(x[2], x[3], x[4],x[5])
        
        if self.boiteVisible == True:
            self.boite.Update()
    
    def Afficher(self, taille, compX, compY):   
        
        if self.boiteVisible == True:
            self.boite.Afficher()
            
            
class UI_OverlayPause():
    
    def __init__(self, renderer):
        self.renderer = renderer
        
        self.background = UI_ElementBackground(renderer, [0,0], 4000, 4000) 
        self.background.bg_colorDefault = sdl2.SDL_Color(200,20,20,120)
        self.texte, self.sizeX, self.sizeY = UI_GenTextureFromText(renderer,   '                Jeu en Pause                  ')
        
        self.bouton1 = UI_Bouton(renderer, "Reprendre", [40,45], event=[['Main', 'Pause', False], ['UI_Overlay', 'PauseShow', False ] ])
        self.bouton2 = UI_Bouton(renderer, "Menu Principal", [40,52], event=[['Main', 'Sauvegarder'], ['UI', 'MenuPrincipalShow', True], ['Main', 'Pause', True],['Son', 'Pause_ResumeMusique',True], ['Son', 'PlayMusique', 'menu1', 100, 0]] )
 
        self.visible = False
        
    def Update(self):
        for x in Events.GetEvents("UI_Overlay"):
            if x[1] == "PauseShow":
                 self.visible = x[2]
                 Events.AddEvent(['Main', 'Pause', x[2]])
                 Events.AddEvent(['Son', 'Pause_ResumeMusique', not x[2]])
                 Events.AddEvent(['Son', 'Pause_ResumeBruitages', not x[2]])


        
        if self.visible == True:
            self.background.Update(hoovered=False)
            self.bouton1.Update()
            self.bouton2.Update()
        

        
    def Afficher(self, taille, compX, compY):
        
        if self.visible == True:
            self.background.Afficher()
            
            model = sdl2.SDL_Rect(int(50/100*Input.resX-self.sizeX/2),int(40/100*Input.resY-self.sizeY/2),self.sizeX, self.sizeY)
            sdl2.SDL_RenderCopy(self.renderer, self.texte, None, model)
            self.bouton1.Afficher()
            self.bouton2.Afficher()



class UI_OverlayMap():
    
    
    def __init__(self, renderer):
        self.renderer = renderer
        self.backgroundTexture  = tx.GenTexture(renderer, 'Map', 'background')
        self.CarteTexture  = tx.GenTexture(renderer, 'Map', 'FondCarte')

        self.visible = False
        
        self.background = UI_ElementBackground(renderer, [0,0], 4000, 4000) 
        self.background.bg_colorDefault = sdl2.SDL_Color(20,20,20,120)     
        self.background.Update()
        
        self.texte, self.sizeX, self.sizeY = UI_GenTextureFromText(renderer,   '                        Carte du monde                     ')

    
    def Update(self):
         
        for x in Events.GetEvents("UI_Overlay"):
            if x[1] == "MapShow":
                 self.visible = not self.visible
      
    def Afficher(self, taille, compX, compY):
        if self.visible == True:
             
            self.background.Afficher()
            model = sdl2.SDL_Rect(int(50/100*Input.resX-self.sizeX/2),int(20/100*Input.resY-self.sizeY/2),self.sizeX, self.sizeY)
            sdl2.SDL_RenderCopy(self.renderer, self.texte, None, model)
            
            rD = Data.GetVariable("renderDistance", Data.Config.data)
            for x in range(15):
                for y in range(8):
                    tx.AfficherTexture(self.renderer, self.backgroundTexture, [((rD-14)/2)+x, y+8], taille, compX, 0)
            
            tx.AfficherTexture(self.renderer, self.CarteTexture, [(rD-14)/2, 8], taille, compX, 0)

        