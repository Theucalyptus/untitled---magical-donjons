#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 21 15:22:56 2019

@author: antoine
"""

import sdl2.ext

from Utils import Input

class UI_Transition():
    
    def __init__(self, renderer, color, speed):
        self.renderer = renderer
        self.color = color
        self.speed = speed
        
        self.timeDebut = -3000
        self.transition = False
    
    def StartAnimation(self):
        self.timeDebut = sdl2.SDL_GetTicks()
        self.transition = True
        
    def EndAnimation(self):
        self.timeDebut = sdl2.SDL_GetTicks()
        self.transition = False
        
    def SetColor(self, color):
        self.color = color
        
    def Afficher(self):
            if self.transition == True:
                model = sdl2.SDL_Rect(0,0, int((sdl2.SDL_GetTicks()-self.timeDebut)/self.speed*Input.resX), Input.resY)     
            else:
                model = sdl2.SDL_Rect(int((sdl2.SDL_GetTicks()-self.timeDebut)/self.speed*Input.resX),0, Input.resX, Input.resY)
            sdl2.SDL_SetRenderDrawColor(self.renderer, self.color.r, self.color.g, self.color.b, self.color.a)
            sdl2.SDL_RenderFillRect(self.renderer, model)
        
        
    