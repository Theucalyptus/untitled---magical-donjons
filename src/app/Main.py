#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  9 8:81:34 2019

@author: antoine
"""

#Pour copier la configuration par défaut en cas de premier démarage
from shutil import copyfile

#pour charger la sauvegarde
import numpy as np

#pour utiliser SDL (fénètre, contexte opengl et évènement (clavier/souris))
import sdl2.ext 

#Import spécific au jeu
import Utils
from Monde import Monde # pour avoir un monde - ca peut etre utile :)
import Joueur
import UI
import Son
from Combat import Combat


def Run() :
   

    #Initialisation de la fentre et du renderer
    fenetre, renderer = Utils.SDL_Gestion.Init(Utils.Input.resX, Utils.Input.resY)

    #Mettre ici le chargement du fichier de configuration     
    def CFGLoad():
        '''
            Charge la configuration du joueur si existante sinon charge la configuration par defaut
        '''
        if Utils.Data.Charger('Data/config.json') == False:
            copyfile('Data/defaultCfg.json', 'Data/config.json')
        
        Utils.Config.data = Utils.Data.Charger("Data/config.json")
    
        Utils.Input.resX, Utils.Input.resY = Utils.Data.GetVariable("resX", Utils.Data.Config.data), Utils.Data.GetVariable("resY", Utils.Data.Config.data)
        Son.SetVolumeMusique(volume=Utils.Data.GetVariable("volumeMusique", Utils.Data.Config.data))
        Son.SetVolumeBruitages(Utils.Data.GetVariable("volumeBruitages", Utils.Data.Config.data))
        sdl2.SDL_SetWindowSize(fenetre, Utils.Input.resX, Utils.Input.resY)
        sdl2.SDL_SetWindowFullscreen(fenetre, Utils.Data.GetVariable("fullScreen", Utils.Data.Config.data))

        Utils.Config.data_buffer = Utils.Config.data

    CFGLoad()


    monde = Monde(renderer)
    joueur = Joueur.Joueur(renderer) 
    ui = UI.UI_Main(renderer)
    combat = Combat(renderer)
      
    
    #Boucle principale (début du jeux)
    while Utils.Input.terminer != True:        
                
        #Mise à jour des evènement et input donner par SDL
        Utils.SwapEvents()
        Utils.UpdateInput()
        
        #Gestion de la résolution dynamique et récupération des valeurs de compensation de résolution (pour maintenir le centrage même avec un changement de résolution)
        size, compX, compY = Utils.SDL_Gestion.UpdateResolution()
        
        #Mise à jour des évènements
        for x in Utils.GetEvents('Main'):
    
            #Mise en pause du jeu
            if x[1] == "Pause":
                Utils.Input.pause = x[2]
                
            #Charge le jeu et la sauvegarde (bouton Jouer du menu principal)
            elif x[1] == "Jouer": 
                
                # Lance les évènements pour masquer les menus et autoriser la lecture de musique
                Utils.Events.AddEvent(["UI", "MenuPrincipalShow", False])
                Utils.Events.AddEvent(["Son", "Pause_ResumeMusique", True])
                Utils.Events.AddEvent(["Son", "Pause_ResumeBruitage", True])
    
                #Chargement de la sauvegarde
                sav = np.load('Data/Sauvegarde/monde.sav.npz', allow_pickle=True)

                # Chargement du monde, du personnage puis lancement du jeu (fin de pause et Affichage des interfaces)
                Utils.Events.AddEvent(["Monde", "Charger", sav['zoneID'], sav['ID'], None])
                Utils.Events.AddEvent(["Joueur", "Charger"])
                Utils.Events.AddEvent(["Main", "Pause", False])
                Utils.Events.AddEvent(["UI", "OverlaysShow", True])

            #Sauvegarde la progression du joueur (monde et personnage)
            elif x[1] == "Sauvegarder":
                monde.Sauvegarder()
                joueur.Sauvegarder()
            
            #Quitte le jeu
            elif x[1] == "Quitter":
                Utils.Input.terminer = True
            
            #Applique la configuration
            elif x[1] == "AppliquerConfig":
                Utils.Config.data=Utils.Config.data_buffer
                Utils.Data.Sauvegarder(Utils.Config.data, "Data/config.json")
                CFGLoad()
        
            
        #Update générale si le jeu n'est pas en pause
        if Utils.Input.pause != True:
            joueur.Update(monde)
            monde.Update()            
        
        combat.Update()
			
        #Fin update partielle même si en pause (update du son et des interfaces)
        ui.Update()
        Son.Update()
        
        #Affichage
        sdl2.SDL_SetRenderDrawBlendMode(renderer, sdl2.SDL_BLENDMODE_BLEND)
        sdl2.SDL_SetRenderDrawColor(renderer,0,0,0, 255)   #coleur du fond pour le debug
        sdl2.SDL_RenderClear(renderer) #suppréssion du buffer de l'écran
              
        monde.Afficher1(size, compX, compY, joueur.coord) #affchiage du terrain
        joueur.Afficher(size, compX, compY) #affichage du sprite du joueur
        monde.Afficher2(size, compX, compY, joueur.coord) #affichage des objets en hauteur + fx météo
        combat.Afficher(size, compX, compY) #affichage du combat ;)
        ui.Afficher(size, compX, compY) #affichage de l'ui
              
        #Update de la fenètre
        sdl2.SDL_RenderPresent(renderer)

                       
    #Sauvegarde et destruction des objets pour libérer la RAM et la VRAM
    print("Fermeture...")
    Utils.Data.Sauvegarder(Utils.Config.data, "Data/config.json")
    
    monde.Sauvegarder()
    monde.Decharger()
    joueur.Sauvegarder()
    joueur.Decharger()
    
    #Destruction des objets SDL
    Utils.SDL_Gestion.Quit(renderer, fenetre)

#Démarrage du programme
Run()
