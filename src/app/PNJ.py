

from Utils import Events, Input, Data
import Texture as tx

class PNJ():
    
    def __init__(self, renderer, ID):
        
        #Déclaratiopn de toutes les variables
        self.ID = ID 
        self.nom = "Théo"
        
        self.texturesArrete = list()
        self.texturesWalking = list()
        
        self.renderer = renderer
        
        self.nbr_phaseAnimation = 4
        
        for x in range(4):
            self.texturesArrete.append(tx.GenTexture(renderer, 'Personnages/PNJ/'+str(ID), 'pnj', x))
            self.texturesWalking.append(tx.GenAnimatedTexture(renderer, 'Personnages/PNJ/'+str(ID), 'pnj' + str(x) + '-', self.nbr_phaseAnimation, 16))

        self.orientation = 0
        self.mooving = False
        self.canMove = True
        self.phaseAnimation, self.quartAnimationTarget = 0, 0
        
        self.targetTexture = self.texturesArrete[self.orientation]
        self.coord = [40,39]
        
        self.actions = list()
        self.targetActions = [['UI_Overlay', 'SetTexte', "Bonjour, je m'appelle " + str(self.nom), self.nom , self.ID, 0], ['Joueur', 'LockMouvement', True]]
        self.waitingEvent = None
        
    def Update(self):
        
        #Update des évènements
        for x in Events.GetEvents("PNJ"):
            if x[1] == "DeclancherAction" and x[2] == self.ID and self.waitingEvent == None:
                Input.touchesClavier[Data.GetVariable('toucheAction', Data.Config.data)] = False
                for y in self.targetActions:
                    Events.AddEvent(y)

                    if [y[0], y[1]] == ["UI_Overlay", "SetTexte"]:
                        self.waitingEvent = ['PNJ', 'FinAction', self.ID, 0]
                        self.targetActions = [['UI_Overlay', 'SetTexte', "Est ce que tu veux etre mon ami pour la vie ?", self.nom , self.ID, 1]]
                
            
            
            elif x[1] == self.waitingEvent[1]:
                if x[1] == "FinAction":
                    if [x[2], x[3]] == [self.waitingEvent[2], self.waitingEvent[3]]:
                        self.waitingEvent = None
                        Events.AddEvent(['PNJ', 'DeclancherAction', self.ID])


            #Si on veut déplacer le joueur
            elif x[1] == "Deplacer":
                self.coord = x[3]
                 #TODO : pour le moment, se contente juste de tééléporter le PNJ
                 #TODO : implementer un algorithme de path-finding puis faire se déplacer le pnj jusqu'à sa destination
                
                
    def Afficher(self, taille, compX, compY, coordJoueur):
        '''
            Affiche le sprite du pnj
        '''              
        if Input.cameraLocked == True:
            tx.AfficherTexture(self.renderer, self.targetTexture, [self.coord[0]+(11-coordJoueur[0]), self.coord[1]+(11-coordJoueur[1])], taille, compX, compY)

        else:
            tx.AfficherTexture(self.renderer, self.targetTexture, self.coord, taille, compX, compY)
