from Utils import Events

import Texture as tx
import numpy as np
import sdl2.ext


class Combat() :

        def __init__(self, renderer) :
                
                self.combatVisible = False
                self.background = None
                self.renderer = renderer
                self.animationTarget = -200
                self.temp = False
                tableau = np.array([[2,    1/2,    1/2,    2,    2,    1/2,    2,    1/2],
                                    [1/2,  2,      1/2,    1/2,  2,    2,      2,    1/2],
                                    [1/2,  1/2,    2,      2,    1/2,  2,      2,    1/2],
                                    [1,    1/4,    1,      4,    1,    1,      4,    1/4],
                                    [1,    1,      1/4,    1,    4,    1,      4,    1/4],
                                    [1/4,  1,      1,      1,    1,    4,      4,    1/4],
                                    [1/2,  1/2,    1/2,    2,    2,    2,      6,    1/6],
                                    [1,    1,      1,      1,    1,    1,      1,    1]
                                    ])
            
    
    
        def Update(self) :
            
            for x in Events.GetEvents("Combat"):
                if x[1] == 'Lancer' :
                    self.animationTarget = sdl2.SDL_GetTicks() + 520
                    self.temp = True
                    Events.AddEvent(["UI", "TransitionStart"])
                    Events.AddEvent(["Joueur", "LockMouvement", True])
                    self.toLoadZone = x[2]
                                            
                    
                if x[1] == 'Quitter':
                    Events.AddEvent(["UI", "TransitionStart"])
                    Events.AddEvent(["Joueur", "LockMouvement", False])
                    self.animationTarget = sdl2.SDL_GetTicks() + 520
                    self.temp = False

                    

            if sdl2.SDL_GetTicks() >= self.animationTarget and self.animationTarget != -200:
                if self.temp ==True:
                    self.background = tx.GenTexture(self.renderer, "Combat/Background", str(self.toLoadZone))
                    self.character = tx.GenAnimatedTexture(self.renderer, "Combat/Personnage", "beowulf", 8, 12, 0) 
                else:
                    tx.DeleteTexture(self.background)
                    tx.DeleteTexture(self.character)
                self.combatVisible = self.temp
                self.temp = False
                self.animationTarget = -200
                Events.AddEvent(["UI", "TransitionEnd"]) 
                
                
        def Afficher(self, taille, compX, compY):
                
                if self.combatVisible == True :
                    tx.AfficherTexture(self.renderer, self.background, [0,0], taille, compX, compY*2, ByPassSize=True)
                    tx.AfficherTexture(self.renderer, self.character, [6,7], taille, compX, compY)
