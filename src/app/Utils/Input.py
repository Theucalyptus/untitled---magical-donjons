#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 11 15:35:27 2019

@author: antoine
"""
import sdl2.ext
import numpy

import Utils

class Input():
    
    terminer = False
    pause = False
    touchesClavier = numpy.full((sdl2.SDL_NUM_SCANCODES), False)
    touchesSouris = numpy.full((8), False)
    sourisX = 0
    sourisY = 0
    sourisRelX = 0
    sourisRelY = 0 
    resX, resY = 800, 600
    
    cameraLocked = False
    waitingForKey = False    

    
def UpdateInput():
    events = sdl2.ext.get_events()
    Input.sourisRelX,Input.sourisRelY=0,0
           
    for event in events:                
        #Si le bouton de fermeture est préssé ou Alt+F4 : ferme la fenètre
        if event.type == sdl2.SDL_QUIT or event.type == sdl2.SDL_APP_TERMINATING: Input.terminer = True 
                   
        #Si une touche est préssé vers le bas
        elif event.type == sdl2.SDL_KEYDOWN : 
            if Input.waitingForKey != False:
                Utils.Data.SetVariable(str(Input.waitingForKey), event.key.keysym.scancode, Utils.Data.Config.data_buffer)
                Input.waitingForKey = False
            else:
                Input.touchesClavier[event.key.keysym.scancode] = True    # on met la valeur correspondante à True
           
        #Si c'est ubne touche que on relache
        elif event.type == sdl2.SDL_KEYUP: Input.touchesClavier[event.key.keysym.scancode] = False #si c'est un relachement on remet à False
                   
        #Si c'est un mouvement de souris           
        elif event.type == sdl2.SDL_MOUSEMOTION: #Si c'est la souris qui bouge
            Input.sourisX,Input.sourisY,Input.sourisRelX,Input.sourisRelY = event.motion.x,event.motion.y,event.motion.xrel,event.motion.yrel
        
        elif event.type == sdl2.SDL_MOUSEBUTTONDOWN:
            Input.touchesSouris[event.button.button] = True
        elif event.type == sdl2.SDL_MOUSEBUTTONUP:
            Input.touchesSouris[event.button.button] = False

        
        #Si c'est en rapport à la fenètre          
        elif event.type == sdl2.SDL_WINDOWEVENT:
            
            #Si c'est la fenètre qui a été redimensionnée
            if event.window.event == sdl2.SDL_WINDOWEVENT_SIZE_CHANGED:  
                Input.resX, Input.resY = event.window.data1, event.window.data2
                Utils.Data.Config.data_buffer['resX'] = Input.resX
                Utils.Data.Config.data_buffer['resY'] = Input.resY


    #Touche de débug pour tester un évènement
    if Input.touchesClavier[sdl2.SDL_SCANCODE_C] :
        Input.touchesClavier[sdl2.SDL_SCANCODE_C] = False
        Utils.Events.AddEvent(['Combat', 'Lancer', 1])
    
    #Touche de débug pour tester un évènement
    if Input.touchesClavier[sdl2.SDL_SCANCODE_V] :
        Input.touchesClavier[sdl2.SDL_SCANCODE_V] = False
        Utils.Events.AddEvent(['Combat', 'Quitter'])
	
    if Input.touchesClavier[Utils.Data.GetVariable('toucheCarte', Utils.Data.Config.data)] :
        Input.touchesClavier[Utils.Data.GetVariable('toucheCarte', Utils.Data.Config.data)] = False
        Utils.Events.AddEvent(['UI_Overlay', 'MapShow'])
    
    if Input.touchesClavier[Utils.Data.GetVariable('toucheRetour', Utils.Data.Config.data)] :
        Input.touchesClavier[Utils.Data.GetVariable('toucheRetour', Utils.Data.Config.data)] = False
        Utils.Events.AddEvent(['Main', 'Pause', False])
        Utils.Events.AddEvent(['UI_Overlay', 'PauseShow', True])

    for x in Utils.Events.GetEvents('Input'):
        if x[1] == 'SetKeyFor':
            Input.waitingForKey = x[2]
