#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 12 10:46:31 2019

@author: antoine
"""

import json
from os.path import isfile

class Config:
       
    data=dict()
    data_buffer=dict()
    
def Charger(path):
    '''
        Charge un fichier json puis le stock en interne
        path est de type str(), contient le chemin relatif de l'excutable au fichier / chemin absolu
    '''
            
    if isfile(path) == True:    
        with open(path) as cfgFile:
            data = json.load(cfgFile) 
            return data
    else : print("Data : Erreur: Le fichier", path, "n'existe pas !")
    return False
    
def GetVariable(nomVariable, data):
        '''
            Retourne la valeur associé à la clé nomVariable (de type str)
        '''
        return Config.data[nomVariable]
                
        
def SetVariable(nomVariable, variable, data):
    '''
        Assigne à la clé nomVariable(de type str) la valeur de variable
    '''
            
    data[nomVariable] = variable
    return data    
    
def Sauvegarder(data, path):
        '''
            Sauvegarde les données/modifications dans le fichier json
        '''
        with open(path, 'w', encoding='utf-8') as cfgFile:
            json.dump(data, cfgFile, ensure_ascii=False, indent = 4)
