#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 22 10:46:17 2019

@author: antoine
"""

class Evenements(): 
    
    eventsMain = list()
    eventsSon = list()
    eventsPNJ = list()
    eventsMonde = list()
    eventsJoueur = list()
    eventsCombat = list()
    eventsUI = list()
    eventsUI_Overlay = list()
    eventsInput = list()
    
    pending = list()

def SwapEvents():
    FlushEvents()   
    
    if len(Evenements.pending) > 0:
        print(Evenements.pending)
    
    for x in Evenements.pending:
        if x[0] == 'Main':
            Evenements.eventsMain.append(x)
        elif x[0] == 'Son':
            Evenements.eventsSon.append(x)
        elif x[0] == 'PNJ':
            Evenements.eventsPNJ.append(x)
        elif x[0] == 'Monde':
            Evenements.eventsMonde.append(x)
        elif x[0] == 'Joueur':
            Evenements.eventsJoueur.append(x)
        elif x[0] == 'Combat':
            Evenements.eventsCombat.append(x)
        elif x[0] == 'UI':
            Evenements.eventsUI.append(x)
        elif x[0] == 'UI_Overlay':
            Evenements.eventsUI_Overlay.append(x)
        elif x[0] == 'Input':
            Evenements.eventsInput.append(x)
            
    Evenements.pending = list()


def FlushEvents():
    Evenements.eventsJoueur, Evenements.eventsMain, Evenements.eventsMonde, Evenements.eventsCombat, Evenements.eventsSon = list(), list(), list(), list(), list()
    Evenements.eventsPNJ, Evenements.eventsUI, Evenements.eventsUI_Overlay, Evenements.eventsInput = list(), list(), list(), list()

def AddEvent(event):
    Evenements.pending.append(event)
    
    
def GetEvents(categorie):
        if categorie == 'Main':
            return Evenements.eventsMain
        elif categorie == 'Son':
            return Evenements.eventsSon
        elif categorie == 'PNJ':
            return Evenements.eventsPNJ
        elif categorie == 'Monde':
            return Evenements.eventsMonde
        elif categorie == 'Joueur':
            return Evenements.eventsJoueur
        elif categorie == 'Combat':
            return Evenements.eventsCombat
        elif categorie == 'UI':
            return Evenements.eventsUI
        elif categorie == 'UI_Overlay':
            return Evenements.eventsUI_Overlay
        elif categorie == "Input":
            return Evenements.eventsInput
        else:
            print("Events: Info : tentative d'accès à une catégorie inexistante :" + categorie)
            
    
    