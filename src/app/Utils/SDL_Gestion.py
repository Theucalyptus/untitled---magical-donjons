#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 19 09:33:42 2019

@author: antoine
"""

import sdl2.ext
from Utils import Input, Data

def Init(resX, resY):
     
    #Initialisation de SDL2
    print("Initialisation de SDL2....")
    if sdl2.ext.init() == 0 : #Si sdl à pas démarrer
        print(sdl2.SDL_GetError())
        return -1 #on quite le programme
    print("Terminée") #sinon on contine

    #Création de la fenètre
    print("Création de la fenètre....")
    window = sdl2.SDL_CreateWindow(b"Untitled - Magical Donjons", sdl2.SDL_WINDOWPOS_UNDEFINED, sdl2.SDL_WINDOWPOS_UNDEFINED, resX, resY, sdl2.SDL_WINDOW_SHOWN | sdl2.SDL_WINDOW_RESIZABLE)
    if window == 0 : #si la fenètre ne c'est pas créer
         print(sdl2.SDL_GetError())
         return -1
    print("Terminée")
    
    #Chargement de SDL_TTF pour gerer le texte:
    print("Initialisation de SDL_TTF...")
    if sdl2.sdlttf.TTF_Init() ==-1:
        print(sdl2.sdlttf.TTF_GetError())
        return -1
    print('Terminée')

    #Chargement de SDL_Mixer pour la musique et les sons
    print("Initialisation de SDL_Mixer...")
    if sdl2.sdlmixer.Mix_Init(sdl2.sdlmixer.MIX_INIT_OGG) != sdl2.sdlmixer.MIX_INIT_OGG:
        print("Erreut au chargememetn de SDL2_Mixer: ", sdl2.sdlmixer.Mix_GetError())
        return -1
    sdl2.sdlmixer.Mix_AllocateChannels(16);
    sdl2.sdlmixer.Mix_OpenAudio(44100, sdl2.sdlmixer.MIX_DEFAULT_FORMAT, 2, 1024)
    print("Terminée")
    
    #Création du renderer sdl associé à la fenètre
    print("Création du renderer SDL2...")
    renderer = sdl2.SDL_CreateRenderer(window, -1, sdl2.SDL_RENDERER_ACCELERATED)
    if renderer == 0 : #si le renderer ne c'est pas créer
        print(sdl2.SDL_GetError())
        return -1
    print("Terminé")


    return window, renderer


def UpdateResolution():
    
    #22 est la render distance, çàd le nombre de block visinle par le joueur
    
    rD = Data.GetVariable("renderDistance", Data.Config.data)
    
    #Mise à jour de la taille en pixel d'un block :        
    if Input.resX >= Input.resY:
         size = Input.resX//rD
         compensationY, compensationX =  int(rD-(Input.resY/size)), 0
    else: 
         size = Input.resY//rD
         compensationX, compensationY = int(rD-(Input.resX/size)), 0

    return size, compensationX, compensationY



def Quit(renderer, window):
    
    #On quitte la sdl
    sdl2.SDL_DestroyRenderer(renderer)
    sdl2.SDL_DestroyWindow(window)
    sdl2.sdlttf.TTF_Quit()
    sdl2.sdlmixer.Mix_Quit()
    sdl2.SDL_Quit()
