

from Utils import Events
from sdl2 import sdlmixer


class SonMain():
    
    musique = None
    musiqueFadingOut = False

    bruitages = list()
    for x in range(16):
        bruitages.append(None)


def Update():
    
    for x in Events.GetEvents('Son'):
         if x[1] == "PlayMusique":
             PlayMusique(x[2], x[3], x[4])
        
         elif x[1] == "StopMusique":
             StopMusique(x[2])
        
         elif x[1] == "SetVolumeMusique":
             SetVolumeMusique(x[2])
             
         elif x[1] == "PlayBruitage":
             PlayBruitage(x[2], x[3], x[4])
             
         elif x[1] == "SetVolumeBruitages":
             SetVolumeBruitages(x[2])
             
         elif x[1] == "Pause_ResumeMusique":
             Pause_ResumeMusique(x[2])
            
         elif x[1] == "Pause_ResumeBruitage":
             Pause_ResumeBruitage(x[2])
    
    
#Règle le volume de la musique
def SetVolumeMusique(volume=100):
    sdlmixer.Mix_VolumeMusic(int(sdlmixer.MIX_MAX_VOLUME*(volume/100)))
    
#Joue une musique
def PlayMusique(fichier, dureeFadeIn=500, dureeFadeOut=500):
    
    if SonMain.musique != None: #Si une musique est en cours de lecture
        if sdlmixer.Mix_PlayingMusic() == 1 and SonMain.musiqueFadingOut == False: #Si la musique en cours de lecture n'est pas en train de se terminer
            sdlmixer.Mix_FadeOutMusic(dureeFadeOut)
            SonMain.musiqueFadingOut = True

        
        elif sdlmixer.Mix_PlayingMusic() == 0 and SonMain.musiqueFadingOut == True: #Si la musique vient de se terminer, préparation pour lecture de la musique demandée
            sdlmixer.Mix_FreeMusic(SonMain.musique)
            SonMain.musique = None
            SonMain.musiqueFadingOut = False

        
        Events.AddEvent(['Son', 'PlayMusique', fichier, dureeFadeIn, dureeFadeOut]) #Relance de l'évènement pour faire comme un boucle mais non bloquante
     
    elif SonMain.musique == None and SonMain.musiqueFadingOut == False: #Si aucune musique n'est en cours de lecture, alors on joue la musique demandée
        SonMain.musique = sdlmixer.Mix_LoadMUS(str("Data/Sons/Musique/" + fichier + ".ogg").encode("utf-8"))
        sdlmixer.Mix_FadeInMusic(SonMain.musique, -1, dureeFadeIn)

def StopMusique(dureeFadeOut=500):
    sdlmixer.Mix_FadeOutMusic(500)
    SonMain.musiqueFadingOut = True


#Règle le volume de lecture des buitages 
def SetVolumeBruitages(volume=100):
    sdlmixer.Mix_Volume(-1, int(sdlmixer.MIX_MAX_VOLUME/(volume/100)))

#Joue un bruitage
def PlayBruitage(categorie, fichier, dureeFadeIn=10):    
    for x in range(16):
        if sdlmixer.Mix_Playing(x) == 0 and SonMain.bruitages[x] != None:
            sdlmixer.Mix_FreeChunk(SonMain.bruitages[x])
            SonMain.bruitages[x] = None
        if SonMain.bruitages[x] == None:    
            SonMain.bruitages[x] = sdlmixer.Mix_LoadWAV(str("Data/Sons/Bruitages/" + categorie + "/" + fichier + ".ogg").encode('utf-8'))
            sdlmixer.Mix_FadeInChannel(x, SonMain.bruitages[x], 0, 0)
            break

#Met en pause / relance la musique
def Pause_ResumeMusique(pause_resume_bool):
    if pause_resume_bool == True:
        sdlmixer.Mix_ResumeMusic()
    elif pause_resume_bool == False:
        sdlmixer.Mix_PauseMusic()
        
        
def Pause_ResumeBruitage(pause_resume_bool):   
    if pause_resume_bool == True:
        sdlmixer.Mix_Resume(-1)
    elif pause_resume_bool == False:
        sdlmixer.Mix_Pause(-1)
            